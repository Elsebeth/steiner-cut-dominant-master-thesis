Evaluating the vertices of subtour polyhedra of small complete graphs 
============

The code in this project was implemented as part of a Master's thesis in mathematics.
The goal was the evaluation of the [vertices of $subtour(K_n)$](https://www.site.uottawa.ca/~sylvia/subtourvertices/index.htm), $6 \le n \le 11$, published by Sylvia Boyd regarding their Steiner degree and operations by which they might be obtained. 

# Setup
Simply copy the repository. Edit and run the python files in the parent folder as needed.

# Structure
- `/csv_data` contains the results of our calculations.
- `/example_graph_plots` contains some examples of graph plots.
- `/functions` contains the functions for reading and writing data, processing the vertices, detecting operations and visualizing the vertices' support graphs.
- `/stat_plots` contains histograms showing the obtainability of certain vertices via the two operations of node and 1-edge splits.
- `/vertices_data` contains the vertices published by Boyd. These are the basis of our calculations.

To access and execute this project's features, use the python files in the parent folder as follows:

# Usage
- `calculate_Steiner_degrees.py`: This is where the Steiner degrees of the vertices are determined. Additionally, for each vertex all possible choices of facet inducing minimal terminal sets are calcuated, as well as the right-hand side in minimum integer form of the inequality each vertex defines. The results are stored in `/csv_data`.
Choose the number $n$ of nodes.

- `operation_evaluation.py`: Determine for each vertex $a$ which of the facet inducing Steiner graphs $(G_a,T)$ with minimal terminal sets $T$ can be the result of a node or $1$-edge split. The results are visualized as barcharts. Uses the results stored in  `/csv_data`.
Choose the number $n$ of nodes. 
<img src="/stat_plots/K_9_deg_8_rhs_4.png" width="50%" height="50%" title="obtainability chart example">

- `plot_graphs.py`: Visualize the Steiner graphs $(G_a,T)$ for some vertex $a$ of subtour$(K_n)$ for all possible choices of minimal $T$. This is done statically (using the [networkx package](https://networkx.org/)) and interactively (using the [pyvis package](pyvis.readthedocs.io/en/latest/)). Uses the results stored in `/csv_data`.
Choose the number $n$ of nodes and the index of the vertex whose support graph you want to visualize.
<img src="/example_graph_plots/K_9_14.png" width="40%" height="40%" title="example graph plot">
<img src="/example_graph_plots/K_9_14_interactive_example.png" width="40%" height="40%" title="example interactive graph plot">

# Contact
For questions or comments, please contact me at <liz_jung@web.de>. 









