# -*- coding: utf-8 -*-
"""
Determine whether the supporting Steiner graphs of the vertices of subtour(K_n)
can be obtained via node splits or 1-edge splits.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from fractions import Fraction

from functions.read_write_functions import get_vertices, get_rhs,\
    get_all_terminal_sets, get_Steiner_degrees 
from functions.operation_functions import check_for_reverse_node_split, \
    check_for_reverse_1_edge_split
    


###############################################################################
# Set your parameters:  

# The number of nodes in our complete graph. Choose a number between 6 and 11.
NODES = 10    

# Only evaluate the vertices  of subtour(K_NODES) which define inequalities 
# with right-hand side rhs_choice in minimum integer form.
# Evaluate all, if this is None.
rhs_choice = None      
                        
# Choose how much output you want.            
verbose = False     

# We proved that node splits are reversible, but we can check whether each
# graph resulting from a reverse node or 1-edge split actually is facet
# inducing to be sure we made no errors.
check_facet_def = False    
                

###############################################################################
###############################################################################



# Read the vertices and the data from our previous computations.
vertices_str = get_vertices('vertices_data/vertices_'+str(NODES)+'.txt')
vertices= [[float(Fraction(c)) for c in v_str] for v_str in vertices_str]

T_list = get_all_terminal_sets(NODES)   # all minimal terminal sets for all 
                                        # vertices
rhs = get_rhs(NODES)
Steiner_degrees = get_Steiner_degrees(NODES)



###############################################################################
# Store the data 
node_splits_all_Ts = []     # For every single terminal set in T_list determine
                            # whether (G_vertex,T) can be obtained via a 
                            # node split.
edge_splits_all_Ts = []     # Same for 1-edge splits.

vertex_from_node_split = []     # For every vertex determine whether there is 
                                # some T in its entry of T_list such that 
                                # (G_vertex, T) is obtainable via a node 
                                # split.
vertex_from_edge_split = []     # Same for 1-edge splits.

nr_node_per_vertex = []     # How many of the terminal options are attainable
nr_edge_per_vertex = []     # via our different operations?
nr_both_per_vertex = []     # We save these for the bar charts.
nr_none_per_vertex = []

unobtainable_vertex_indices = []    # Save the indices for which some T was 
                                    # unobtainable                  


###############################################################################
# Evaluate the vertices w.r.t. their obtainability from node and 1-edge splits.

for index, vertex in enumerate(vertices):

    if rhs_choice is not None and rhs[index] != rhs_choice:
        continue
    if verbose:
        print(index)
        
    node_split_vertex = False  # can the vertex be obtained via a node split?
    edge_split_vertex = False  # can the vertex be obtained via a 1 edge split?
    vertex_Ts = T_list[index]  # get the terminal options for this vertex
    
    node_splits_current_Ts = []
    edge_splits_current_Ts = []
    
    
    for T in vertex_Ts:
        if verbose:
            print("T=", T)
        
        
        # check for node splits that might have occurred
        node_split_reversible = check_for_reverse_node_split(vertex, T, NODES, 
                                            verbose = verbose,
                                            verify = check_facet_def)
        node_splits_current_Ts.append(node_split_reversible)
        if node_split_reversible:
            node_split_vertex = True
        
      
        # check for edge splits that might have occurred    
        edge_split_reversible = check_for_reverse_1_edge_split(vertex, T, 
                                            NODES, verbose = verbose, 
                                            verify = check_facet_def)
        edge_splits_current_Ts.append(edge_split_reversible)
        if edge_split_reversible:
            edge_split_vertex = True
        
                        
        # output the unobtainable Steiner graphs
        if not edge_split_reversible and not node_split_reversible:
            print('\n', index, "T=", T, "not obtainable!")
            print("rhs:", rhs[index], 
                  "Steiner degree:", Steiner_degrees[index])
            unobtainable_vertex_indices.append(index)
            
            
    ###########################################################################
    # collect the results
    
    only_node_split = [i and not j for i,j in zip(node_splits_current_Ts, 
                                              edge_splits_current_Ts)]
    only_edge_split = [j and not i for i,j in zip(node_splits_current_Ts, 
                                              edge_splits_current_Ts)]     
    both_operations = [i and j for i,j in zip(node_splits_current_Ts, 
                                              edge_splits_current_Ts)]
    at_least_one_operation = [i or j for i,j in zip(node_splits_current_Ts, 
                                              edge_splits_current_Ts)]
    no_operation = [not i and not j for i,j in zip(node_splits_current_Ts, 
                                              edge_splits_current_Ts)]
    
    nr_both_per_vertex.append(both_operations.count(True))
    nr_node_per_vertex.append(only_node_split.count(True))
    nr_edge_per_vertex.append(only_edge_split.count(True))
    nr_none_per_vertex.append(no_operation.count(True))
     
    if verbose:
        print("Steiner degree ", len(T), ", terminal sets: ", len(vertex_Ts))
        print("both operations possible: ", both_operations.count(True))
        print("only node split possible: ", only_node_split.count(True))
        print("only edge split possible: ", only_edge_split.count(True))
        print("neither possible: ", no_operation.count(True))
                
    node_splits_all_Ts.append(node_splits_current_Ts)
    edge_splits_all_Ts.append(edge_splits_current_Ts)   

    vertex_from_node_split.append(node_split_vertex) 
    vertex_from_edge_split.append(edge_split_vertex)        
 



###############################################################################
# visualize the results

def plot_barchart(Steiner_degree, rhs_choice):
    
    indices_deg = [i for i, (d,l) in enumerate(zip(Steiner_degrees, rhs)) if 
                    d == Steiner_degree and l ==rhs_choice]
    relevant_number = len(indices_deg) # how many vertices have this combina-
                                       # tion of Steiner degree and rhs?
    if relevant_number==0:
        return                  
    
    unobtainable_number = len([i for i in indices_deg if 
                              i in unobtainable_vertex_indices])
    
    print('Of', relevant_number, 'vertices of right-hand side', rhs_choice,
          'and Steiner degree', Steiner_degree, ',', 
          unobtainable_number, 'were unobtainable via node and',
          '1-edge split; ', 
          round(100*(relevant_number-unobtainable_number)/(relevant_number), 
                2), 
          '% were obtainable' )
    
    # make the visualization more compact: we plot the bars of all vertices 
    # with our desired degree and rhs directly next to each other regardless 
    # of their indices
    x = range(len(indices_deg))
        
    y1 = np.asarray([nr_both_per_vertex[evaluated_indices.index(i)] 
                     for i in indices_deg])
    y2 = np.asarray([nr_node_per_vertex[evaluated_indices.index(i)] 
                     for i in indices_deg])
    y3 = np.asarray([nr_edge_per_vertex[evaluated_indices.index(i)] 
                     for i in indices_deg])
    y4 = np.asarray([nr_none_per_vertex[evaluated_indices.index(i)] 
                     for i in indices_deg])
    
    # custom bar width
    w = (0.8 if relevant_number>5 else 0.5)

    fig = plt.gcf()
    fig.set_size_inches(6, 3)
    if relevant_number<5:
        fig.set_size_inches(4, 3)
    if relevant_number>50:
        fig.set_size_inches(10, 3)
    if Steiner_degree == NODES:
        fig.set_size_inches(6, 2)
    if relevant_number<5:
        fig.set_size_inches(4, 3)
        
    # plot bars in stack manner
    plt.bar(x, y1, color='royalblue', width = w)
    plt.bar(x, y2, bottom=y1, color='plum', width = w)
    plt.bar(x, y3, bottom=y1+y2, color='sandybrown', width = w)
    plt.bar(x, y4, bottom=y1+y2+y3, color='crimson', width = w)
    
    # configure the ticks and labels
    ax = plt.gca()
    
    if relevant_number<5:
        ax.margins(x=(5-relevant_number)*0.05, y=0)
    else:
        ax.margins(x=0.02, y=0)
    
    ax.tick_params(axis = 'y', which='major', labelsize = 10)
    ax.tick_params(axis = 'x', which='major', direction='out', length=5, 
                   labelsize = 10, labelrotation = 60)
    ax.tick_params(which='minor', width=0, length=0)
    
    # make the y axis ticks integer
    ax.yaxis.set_major_locator(ticker.MaxNLocator(integer=True))
    
    # we want labels only for the vertices that have an unobtainable facet
    # inducing terminal set 
    majors = [n for n in range(relevant_number) if # there are unobtainable Ts
              nr_none_per_vertex[evaluated_indices.index(indices_deg[n])]>0] 
    ax.set_xticks(majors, [indices_deg[n] for n in majors])
    ax.xaxis.set_major_locator(ticker.FixedLocator(majors))
    minors = [n for n in range(relevant_number) if 
              nr_none_per_vertex[evaluated_indices.index(indices_deg[n])]==0]
    ax.xaxis.set_minor_locator(ticker.FixedLocator(minors))

    # titles and labels
    plt.xlabel("vertices", fontsize = 11, labelpad=0)
    plt.ylabel("number of minimal terminal sets", fontsize = 11)
    plt.legend(["both", "node split", "edge split", "neither"], 
               loc='upper right', fontsize = 11)
    plt.title(str(relevant_number)+" vertices of subtour"+r'$(K_{' + str(NODES) + '})$'+
              "\n with Steiner degree "+ str(Steiner_degree)+ 
              " and right-hand side "+ str(rhs_choice), fontsize = 12)
    
    # save the plot
    filename = ('K_'+str(NODES)+'_deg_'+str(Steiner_degree) +'_rhs_'+
                str(rhs_choice))
    plt.savefig('stat_plots/'+filename+'.png', dpi=300, bbox_inches="tight")
    plt.show()
    
    return




###############################################################################
'''
Produce the bar charts.
The ticks become very cluttered for some combinations of nodes, Steiner degrees
and right-hand sides, so this particular visualization is really only suited 
to some of them.
'''
print('\n')

# the indices of the vertices we evaluated, i.e. of vertices with right-hand 
# side rhs_choice. This is used for the bar charts.
if rhs_choice is None:
    evaluated_indices = [i for i in range(len(vertices))]
else:
    evaluated_indices = [i for i in range(len(vertices)) 
                         if rhs[i] == rhs_choice]

# the right-hand sides of the vertex-defined inequalities for wich we evaluated
rhs_options = ([rhs_choice] if rhs_choice is not None else 
               list(set(rhs)))

# one bar chart for each combination of rhs and Steiner degree
for r in rhs_options:
    for i in range(6, NODES+1):
        plot_barchart(i, r)



