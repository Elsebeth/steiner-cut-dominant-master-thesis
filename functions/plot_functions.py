# -*- coding: utf-8 -*-
"""
Functions for plotting graphs.
"""
import os
import webbrowser
from itertools import combinations
from pyvis.network import Network
import networkx as nx
import matplotlib.pyplot as plt


from functions.evaluation_functions import edgelist_index




###############################################################################
# Interactive graphs
###############################################################################

def interactive_graph(vertex, vertex_str, NODES, T, show_edge_labels = False, 
                      highlight_1_edges = False):
    '''
    Interactive support graph of 'vertex'.

    Parameters
    ----------
    vertex : list of floats
        Edge weights on K_'NODES'.
    vertex_str : list of strings
        Edge weights on K_'NODES'. Used for labeling.
    nodes : int
        Number of nodes.
    T : list of ints
        List of the terminal nodes.
    show_edge_labels : bool, default False
        If this is true, the edge weights are displayed permanently. 
    highlight_1_edges : bool, default False
        If this is true, the edges of weight 1 are coloured blue. 

    Returns
    -------
    None.

    '''
    net = Network(height='600px', width='600px', bgcolor='white', 
                  font_color='black')
    
    # define the edges
    sources = []
    targets = []
    weights = []
    counter = 0
    for i in range(0, NODES):
        for j in range(i+1, NODES):
            if vertex[counter] > 0:
                sources.append(str(i))
                targets.append(str(j))
                weights.append(vertex_str[counter])
            counter += 1
    edge_data = zip(sources, targets, weights)
    
    # build the graph
    for e in edge_data:
        src = e[0]
        dst = e[1]
        w = e[2]
        color = 'black'
        if highlight_1_edges and w=='1':  # highlight 1-edges
            color = 'mediumblue'
        net.add_node(src, src, title=src, shape='circle', 
                     color='grey' if int(src) in T else 'lightgray')
        net.add_node(dst, dst, title=dst, shape='circle', 
                     color='grey' if int(dst) in T else 'lightgray')
        net.add_edge(src, dst, title=w, color=color, width=2, 
                     label = w if show_edge_labels else '')
    
        
    # physics settings
    net.options.edges.smooth.enabled = False
    net.toggle_physics(False)
    
    # save and display
    net.save_graph('mygraph.html')
    #net.show('mygraph.html')
    webbrowser.open('file://' + os.path.realpath('mygraph.html'))
    return

    

###############################################################################
# Non interactive graphs
###############################################################################
    
def draw_graph(vertex, vertex_str, T, NODES, nr_vertex = None,
               position = None, title = True):
    '''
    Draw the support graph of 'vertex' on 'NODES' nodes, highlighting the 
    terminal nodes.

    Parameters
    ----------
    vertex : list of floats
        Defines our edge weights.
    vertex_str : list of strings
        Defines our edge weights. Used for labeling.
    T : list of ints
        List of the terminal nodes.
    nodes : int
        Number of nodes in the graph.
    nr_vertex : int, default None
        The index of 'vertex' in the list of vertices for K_'NODES'.
    position : positions, default None
        Node positions for our graph plot.
    title : bool, default True
        Display the vertex and its Steiner degree as the title?

    Returns
    -------
    None.

    '''
    # differentiate between terminal and non-terminal nodes
    terminal_set = set(T)
    non_terminal_set = set(range(NODES))-terminal_set

    G = nx.Graph()
    
    # add the nodes
    G.add_nodes_from(range(NODES))

    # add the edges
    all_edges = list(combinations(range(NODES), 2))
    for edge in all_edges:
        # if the vertex has a nonzero entry at the index of the edge, 
        # add that edge
        idx = edgelist_index(edge[0], edge[1], NODES)
        if vertex[idx]>0:
            G.add_edge(edge[0], edge[1], weight = vertex_str[idx])
        

    plt.figure(1,figsize=(6,6)) 
    
    # choose the layout
    is_planar, P = nx.check_planarity(G)
    if is_planar: 
        print('The graph is planar.')
        if position == None:
            print("Choosing planar layout.")
            position = nx.planar_layout(G)    
        else:
            print('Choosing custom layout.')
    else: 
        print('The graph is not planar.')
        if position==None:
            print("Choosing circular layout.")
            position = nx.circular_layout(G)
        else:
            print('Choosing custom layout.')

    # nodes
    nx.draw_networkx_nodes(G, position, nodelist=terminal_set, 
                           node_color="gray", edgecolors = "black",
                           node_size=800)
    nx.draw_networkx_nodes(G, position, nodelist=non_terminal_set, 
                           node_color="lightgray", edgecolors = "black",
                           node_size=800)

    # edges
    nx.draw_networkx_edges(G, position, width=4, edge_color = "black")

    # node labels
    nx.draw_networkx_labels(G, position, font_size=20, 
                            font_family="sans-serif")

    # edge labels
    edge_labels = nx.get_edge_attributes(G, "weight")
    nx.draw_networkx_edge_labels(G, position, edge_labels,font_size =18)
    

    ax = plt.gca()
    ax.margins(0.08)
    plt.axis("off")
    # split the title for better layout
    if title:
        index =  "?" if nr_vertex is None else str(nr_vertex)
        plt.title(r"$K_{" + str(NODES) + "}[" + index+ 
                  "]$ has Steiner degree "+str(len(T)), fontsize = 18)
    plt.tight_layout()
    plt.show() 
    return


