# -*- coding: utf-8 -*-
"""
Functions for reading and writing files.
"""

import csv
from fractions import Fraction


###############################################################################
# Functions for getting the vertices of subtourG)
###############################################################################        


def get_vertices(filename, nr_vertex = None):    
    '''
    Get the vertices from 'filename' in a list with strings as the 
    vertex coordinates.

    Parameters
    ----------
    filename  : str
        The is the name of a file which contains one vertex in each line with 
        coordinates separated by spaces.
        
    nr_vertex : int
        None by default. If this is specified, the single vertex in line 
        'nr_vertex' of 'filename' is returned (starting with 0). 
        Otherwise all vertices in 'filename' are returned.

    Returns
    -------
    vertices  : list of lists of str
        List of the vertices. Each vertex is saved as a list.
    '''

    file1 = open(filename, 'r')
    Lines = file1.readlines()
    if nr_vertex is not None and nr_vertex >= len(Lines):
        print('Choose a nr_vertex between 0 and', len(Lines)-1)

    vertices = []  
    
    count = 0
    for line in Lines:
        # Strip the newline character if there is any
        if not line.strip() in (None, ''):
            count += 1
            
            # save the vertex coordinates in a list
            if nr_vertex is None or count == nr_vertex+1 :
                vertex = []
                for coeff in line.split():
                    vertex.append(str(Fraction(coeff)))
                # save the vertex coordinate list for each of the vertices
                vertices.append(vertex)
            if nr_vertex is not None and count ==nr_vertex+1:
                return vertex
            
    return vertices




###############################################################################
# Functions for saving data
###############################################################################

   

def write_csv(filename, data, header=None):
    '''
    Save 'data' in 'filename'.csv
    
    Parameters
    ----------
    filename : string
        Name of the save file.
    data     : iterable of iterables
        The data we want to save.
    header   : iterable of strings
        Headers for the columns of our file. None by default. 

    Returns
    -------
    None.

    '''
    with open(filename, 'w', newline="") as file:
        csvwriter = csv.writer(file)    # create a csvwriter object
        if header is not None: 
            csvwriter.writerow(header)  # write the header
        csvwriter.writerows(data)       # write the rest of the data



###############################################################################
# Functions for reading data we saved
###############################################################################


# # TODO: Ich glaube, diese Funktion ist überholt. Überprüfen!
# def read_T(NODES, nr_vertex=None):  
#     '''
#     Get the terminal node sets

#     Parameters
#     ----------
#     NODES : int
#         number of nodes in our graph.
#     nr_vertex : int
#         None by default. If this is specified, only the Steiner set 
#         corresponding to the vertex with the index 'nr_vertex' is returned. 
#         Otherwise one Steiner set is returned for each of our vertices of 
#         subtour(K_'NODES').

#     Returns
#     -------
#     T_list : list of lists
#         List of the terminal sets. Each terminal set is saved as a list.

#     '''
#     filename = 'csv_data/K_'+str(NODES)+'_Terminal_sets.csv'
#     file1 = open(filename, 'r')
#     Lines = file1.readlines()
#     T_list = []  
    
#     count = 0
#     # Strips the newline character
#     for line in Lines:
#         if not line.strip() in (None, ''):
#             count += 1
#             # save the terminal nodes in a list
#             if nr_vertex is None or count == nr_vertex+1 :
#                 T = []
#                 # remove "( at the beginning and )" at the end
#                 line = line[2:len(line)-3]
#                 for entry in line.split(', '):
#                     T.append(int(entry))
#                     # save the vertex coordinate list for each of the vertices
#                 T_list.append(T)
#             if nr_vertex is not None and count ==nr_vertex+1:
#                 return T_list
#     return T_list


# # TODO: überall durch get_all_terminal_sets ersetzen! 
# # Und sicherstellen, dass es gleich funktioniert!
# def read_all_T_options(NODES, nr_vertex):  
#     '''
#     Get all terminal sets T for which the vertex of subtour(K_'NODES') with 
#     index 'nr_vertex' defines a facet of CUT_+(K_'NODES', T).

#     Parameters
#     ----------
#     NODES : int
#         Number of nodes in our graph.
#     nr_vertex : int
#         The index of our vertex in the list of vertices of subtour(K_'NODES').

#     Returns
#     -------
#     T_list : list of lists
#         List of terminal node sets.

#     '''
#     filename = 'csv_data/K_'+str(NODES)+'_Terminal_sets_all_options.csv'
    
#     # Using readlines()
#     file1 = open(filename, 'r')
#     Lines = file1.readlines()
#     line = Lines[nr_vertex]
#     T_list = []  
    
#     # Strips the newline character
#     if not line.strip() in (None, ''):
#         #print("Terminal sets:",  line.strip())
#         # save the terminal nodes in a list
#         T = []
#         active = False
#         for entry in list(line):
#             #print(entry)
#             if entry=="[" or entry=="]":
#                 active = False
#             if entry ==")":
#                 active = False
#                 T_list.append(T)    
#             if active and entry != "," and entry !=" ":
#                 T.append(int(entry))      
#             if entry=="(":
#                 active = True
#                 T=[]         
#     return T_list



def get_all_terminal_sets(NODES, nr_vertex = None):  
    '''
    For each vertex of subtour(K_'NODES') get all terminal sets T for which 
    the respective vertex defines a facet of CUT_+(K_'NODES', T).

    Parameters
    ----------
    NODES : int
        Number of nodes in our graph.
    nr_vertex : int, optional
        None by default. If 'nr_vertex' is specified, we return all minimal 
        terminal sets for the vertex with that index in our list of vertices 
        of subtour(K_'NODES').
        If 'nr_vertex' is not specified, we return all minimal terminal sets 
        for all entries in our list of vertices of subtour(K_'NODES'). 

    Returns
    -------
    T_list : list of lists of lists
        Contains one list of terminal node sets for each of our vertices of
        subtour(K_'NODES').

    '''
    filename = 'csv_data/K_'+str(NODES)+'_Terminal_sets_all_options.csv'
    
    # Using readlines()
    file1 = open(filename, 'r')
    Lines = file1.readlines()
    if nr_vertex != None:
        Lines = [Lines[nr_vertex]]
        
    T_list = []  
    for line in Lines:
        line = line[2:len(line)-3]  # strip surrounding brackets
        vertex_Ts = []
        # Strips the newline character
        if not line.strip() in (None, ''):
            # save the terminal sets in a list each
            for entry in line.split('('):   
                T = []
                if not entry in (None, ''):
                    entry = entry.replace(')', '')
                    for e in entry.split(', '):
                        if not e in (None, ''):
                            T.append(int(e))
                    vertex_Ts.append(T)
        T_list.append(vertex_Ts)               
    return T_list if nr_vertex == None else T_list[0]


def get_rhs(NODES, nr_vertex = None):    
    '''
    Get the right-hand side of the inequalities defined by the vertices of 
    subtour(K_'NODES') in minimum integer form. 
    If 'nr_vertex' is specified, this function returns only the right-hand side
    for that particular inequality.

    Parameters
    ----------
    NODES : int
        Number of nodes in our graph.
        
    nr_vertex : int
        None by default. If this is specified, the right-hand side of the 
        inequality defined by the vertex in line 'nr_vertex' of the list of 
        Boyd's vertices is returned. Otherwise all right-hand sides 
        of vertices of subtour(K_'NODES') are returned (up to isomorphism).

    Returns
    -------
    rhs  : list of ints
        List of the right-hand sides in minimum integer form.
    '''
    filename = 'csv_data/K_'+str(NODES)+'_rhs_min_int_form.csv'
    file1 = open(filename, 'r')
    Lines = file1.readlines()
    if nr_vertex is not None and nr_vertex >= len(Lines):
        print('Choose a nr_vertex between 0 and', len(Lines)-1)

    rhs = []  
    
    count = 0
    for line in Lines:
        # Strip the newline character if there is any
        if not line.strip() in (None, ''):
            count += 1
            
            # save the right hand sides in a list
            if nr_vertex is None:
                rhs.append(int(float(line)))
            if nr_vertex is not None and count ==nr_vertex+1:
                return int(float(line))      
    return rhs


def get_Steiner_degrees(NODES, nr_vertex = None):    
    '''
    Get the Steiner degrees of the vertices of subtour(K_'NODES'). 
    If 'nr_vertex' is specified, this function returns only the right-hand side
    for that particular inequality.

    Parameters
    ----------
    NODES : int
        Number of nodes in our graph.
        
    nr_vertex : int
        None by default. If this is specified, the Steiner degree of the 
        vertex in line 'nr_vertex' of the list of Boyd's vertices is returned. 
        Otherwise all Steiner degrees of vertices of subtour(K_'NODES' are 
        returned (up to isomorphism).

    Returns
    -------
    Steiner_degrees  : list of ints
        List of the Steiner degrees.
    '''
    filename = 'csv_data/K_'+str(NODES)+'_Steiner_degrees.csv'
    file1 = open(filename, 'r')
    Lines = file1.readlines()
    if nr_vertex is not None and nr_vertex >= len(Lines):
        print('Choose a nr_vertex between 0 and', len(Lines)-1)

    Steiner_degrees = []  
    
    count = 0
    for line in Lines:
        # Strip the newline character if there is any
        if not line.strip() in (None, ''):
            # print("Line'}: '}".format(count, line.strip()))
            count += 1
            
            # save the right hand sides in a list
            if nr_vertex is None:
                Steiner_degrees.append(int(float(line)))
            if nr_vertex is not None and count ==nr_vertex+1:
                return int(float(line))      
    return Steiner_degrees