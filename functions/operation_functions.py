# -*- coding: utf-8 -*-
"""
Functions for determining wheter the supporting Steiner graph of a vertex of 
subtour(K_n) can be obtained via a node split or a 1-edge split.
Keep in mind this is tailored to subtour vertices and WILL need to be 
modified for general vertices of SUB_+(K_n) !
"""

import numpy as np
from itertools import combinations

from functions.evaluation_functions import edgelist_index, min_only, \
        get_T_Steiner_Cuts, get_min_weight



###############################################################################
# helping functions
###############################################################################


def get_neighbours(vertex, node, NODES, weight = None):
    '''
    Get a list of the neighbours of 'node' in the support graph of 'vertex'.

    Parameters
    ----------
    vertex : list of floats
        A vertex of subtour(K_'NODES').
    node : int
        A node in the support graph. 0 <= 'node' <= 'NODES'-1
    NODES : int
        Number of nodes in the graph.
    weight : float, default None
        Option to return only incident edges with this weight.

    Returns
    -------
    neighbours : list of ints
        The nodes in the support graph of 'vertex' that are connected to 
        'node' by an edge of weight 'weight'. Or an edge of any weight >0, 
        if 'weight' is not specified.
    '''
    neighbours = []
    for u in range(NODES):
        if u== node:
            continue
        edgeweight = vertex[edgelist_index(node, u, NODES)]
        if edgeweight ==  weight:    # check for given weight
            neighbours.append(u)
        elif weight == None and edgeweight > 0:
            neighbours.append(u)    # check for >0 if no weight is given
    return neighbours



def terminal_1_edge(vertex, T, NODES):
    '''
    Find all 1-edges connecting two terminal nodes in the support graph of 
    'vertex'.

    Parameters
    ----------
    vertex : list of floats
        A vertex of subtour(K_'NODES').
    T : list of ints
        A terminal node set.
    NODES : int
        Number of nodes in the graph.

    Returns
    -------
    edgelist : list of tuples of ints
        A list of the 'vertex'-weight 1 edges in K_'NODES' whose endpoints
        are both in 'T'.
    '''
    edgelist = []
    edges_two_terminals = list(combinations(T, 2))
    for edge in edges_two_terminals:
        if vertex[edgelist_index(edge[0], edge[1], NODES)]==1:
            edgelist.append(edge)
    return edgelist



def edge_contraction(vertex, T, edge, NODES):
    '''
    Calculate the edge weights that result from those in 'vertex' if we 
    contract the edge 'edge' in the support graph of 'vertex'. 
    Also returns the terminal node set that results from performing
    the contraction from 'T'.
    This function can be used to reverse both node and 1-edge splits.
    We know that under the right circumstances the Steiner graph resulting
    from the contraction is facet inducing with facet weights obtained from 
    the contraction and the same right-hand side.

    Parameters
    ----------
    vertex : list of floats
        A vertex of subtour(K_'NODES').
    T : list of ints
        A set of terminal nodes.
    'edge' : tuple of two ints
        The edge we want to contract.
    NODES : int
        Number of nodes in our graph.

    Returns
    -------
    contracted_graph : list of floats
        Edge weights that result from 'vertex' by performing the contraction.
    new_T : list of ints
        Terminal nodes that result from 'T' by performing the contraction.

    '''
    # contract the node with the larger number into that with the smaller 
    # number
    eater = min(edge[0], edge[1])
    eaten = max(edge[0], edge[1])
    contracted_graph = []
    for i in range(NODES):
        for j in range(i+1, NODES):
            if i!= eaten and j!= eaten:
                contracted_graph.append(
                    vertex[edgelist_index(i,j, NODES)])
            if i == eaten and j!= eater:
                # adjust the node index in the contracted graph
                adjust = (j if j<eaten else j-1)
                contracted_graph[edgelist_index(eater, adjust, NODES-1)] += \
                    vertex[edgelist_index(i, j, NODES)]
            if j == eaten and i!= eater:
                # adjust the node index in the contracted graph
                adjust = (i if i<eaten else i-1)
                contracted_graph[edgelist_index(eater, adjust, NODES-1)] += \
                    vertex[edgelist_index(i, j, NODES)]
    
    # we adjust the terminal set in the contracted graph
    new_T = []
    for i in T:
        if i < eaten:
            new_T.append(i)
        if i == eaten:
            new_T.append(eater)
        if i > eaten:
            new_T.append(i-1)
    
    return contracted_graph, new_T


    
def defines_facet(vertex, T, NODES, min_weight = None, verbose = False):
    '''
    Check if among the characteristic vectors of 'T'-Steiner cuts in the 
    support graph of 'vertex' there are as many linearly independent ones of 
    minimum 'vertex'-weight 'min-weight' as the support graph has edges.
    I.e. check if ('vertex', 'T') is a facet inducing graph.

    Parameters
    ----------
    vertex : list of floats
        A vertex of subtour(K_'NODES').
    T : list of ints
        A terminal node set.
    NODES : int
        Number of nodes in our graph.
    min_weight : float, default None
        None by default. The minimum weight possible for our cuts. 
        If none is given, we calculate the minimum weight of a 'T'-Steiner cut 
        in the graph ourselves.
    verbose : bool, default False
        Determines how much output is wanted.

    Returns
    -------
    bool
        True, if there are sufficiently many linearly independent 
        characteristic vectors of 'vertex'-minimum cuts in our list.
        False otherwise.
    min_cuts : list of lists with 0/1 entries
        A list of the characteristic vectors from 'Char_list' which have 
        minimum 'vertex'-weight.

    '''
    Char_list = get_T_Steiner_Cuts(NODES, T)  # get the characteristic vectors
                                              # of all T-Steiner cuts in K_n
    
    non_0 = np.count_nonzero(vertex)
    non_0_idx = np.nonzero(vertex)  # we need linear independence not in K_n
                                    # but in the support graph of vertex
    if min_weight == None:
        min_weight = get_min_weight(Char_list, vertex)
    min_cuts = min_only(Char_list, vertex, min_weight)
    
    if len(min_cuts)>=non_0:
        M = np.stack([[c[i] for i in non_0_idx][0] for c in min_cuts])
        rank = np.linalg.matrix_rank(M)
        if verbose:
            print("The minimum weight is", min_weight)
            print("We have", len(min_cuts), "minimum cuts and", non_0,"edges.")
            print("We have", rank, "linearly independent minimum cuts.")
        if rank == non_0:
            return True, min_cuts
    return False, min_cuts






###############################################################################
# reverse node split
###############################################################################

def eligible_for_reverse_node_split(vertex, T, NODES):
    '''
    Get all edges in the support graph of 'vertex' with terminal node set 'T'
    that are eligible for an edge contraction.
    These are all 1-edges with one terminal and one non-terminal endpoint that 
    have no common neighbours. 
    The edge must have a terminal and a non-terminal endpoint due to the fact 
    that a node split allows one of the resulting two nodes to be a 
    non-terminal node and our T is a minimal terminal set such that 
    (G_'vertex', T) is facet inducing.

    Parameters
    ----------
    vertex : list of floats
        A vertex of subtour(K_'NODES').
    T : list of ints
        A set of terminal nodes.
    NODES : int
        Number of nodes in our graph.

    Returns
    -------
    eligible_edges : list of tuples of ints
        A list of 'vertex'-weight 1 edges with one terminal and one 
        non-terminal endpoint that have no common neighbours.

    '''
    T_set = set(T)
    non_T_set = set(range(NODES))-T_set
    eligible_edges = []
    for n in non_T_set:
        for t in T:
            # for minimum Ts, a node split results in a non-terminal node 
            # connected by a 1-edge to a terminal node.
            if vertex[edgelist_index(n, t, NODES)]==1:
                # check that n and t have no common neighbours
                t_neighbours = set(get_neighbours(vertex, t, NODES))
                n_neighbours = set(get_neighbours(vertex, n, NODES))
                if not t_neighbours & n_neighbours:
                    eligible_edges.append((n,t))
    return eligible_edges   





def check_for_reverse_node_split(vertex, T, NODES, verbose = False, 
                               verify = False):
    '''
    Check if the support graph of 'vertex' with terminal set 'T' might have 
    occured from a node split. 

    Parameters
    ----------
    vertex : list of floats
        A vertex of subtour(K_'NODES').
    T : list of ints
        A set of terminal nodes.
    NODES : int
        Number of nodes in our graph.
    verbose : bool, deafult False
        Choose how much output you want.
    verify : bool, default False
        We proved that all node pairs that might conceivably be the result of 
        a node split are actually the result of a node split. If 'verify' is 
        True, we nonetheless check if the reverse node split actually yields
        a facet inducing graph to be sure we made no errors in the 
        implementation. 

    Returns
    -------
    node_split_reversible : bool
        States whether (G_'vertex', 'T') results from a smaller facet inducing 
        Steiner graph via a node split.
    '''
    node_split_reversible = False
    
    contractible_edges = eligible_for_reverse_node_split(vertex, T, NODES)

    if len(contractible_edges)>0:
        node_split_reversible = True
        
    for edge in contractible_edges:
        if verbose:
            print("    contractible for edge", edge)
         
        if verify:   
            contracted_graph, new_T = edge_contraction(vertex, T, edge, NODES)
            if not defines_facet(contracted_graph, new_T, NODES-1):
                print("Error_contr")
                break
            
    return node_split_reversible



###############################################################################
# reverse 1-edge split
###############################################################################

def eligible_for_reverse_1_edge_split(vertex, T, NODES):
    '''
    Find the 1-edges connecting two terminal nodes v, v' that have exactly one 
    common neighbour w such that 'vertex'(vw) + 'vertex'(v'w) = 1. 
    These are exactly the results of a 1-edge split.

    Parameters
    ----------
    vertex : list of floats
        A vertex of subtour(K_'NODES').
    T : list of ints
        A set of terminal nodes.
    NODES : int
        Number of nodes in our graph.

    Returns
    -------
    eligible_edges : list of tuples
        List of all 1-edges in G_'vertex' that can be the result of a 1-edge
        split.

    '''
    eligible_edges = []
    edges_two_terminals = list(combinations(T, 2))
    for edge in edges_two_terminals:
        if vertex[edgelist_index(edge[0], edge[1], NODES)]==1:
            t_1_neighbours = get_neighbours(vertex, edge[0], NODES)
            t_2_neighbours = get_neighbours(vertex, edge[1], NODES)
            neighbour_intersection = list(set(t_1_neighbours) & 
                                          set(t_2_neighbours))
            if  len(neighbour_intersection) ==1:
                n = neighbour_intersection[0]
                # the sum of the two edges ending at the common neighbour
                # must be 1
                if (vertex[edgelist_index(edge[0], n, NODES)] + 
                    vertex[edgelist_index(edge[1], n, NODES)]==1):
                    eligible_edges.append(edge)
    return eligible_edges



def check_for_reverse_1_edge_split(vertex, T, NODES, verbose = False, 
                                   verify = False):
    '''
    Check if the support graph of 'vertex' with terminal set 'T' might have 
    occured from a 1-edge split. 

    Parameters
    ----------
    vertex : list of floats
        A vertex of subtour(K_'NODES').
    T : list of ints
        A set of terminal nodes.
    NODES : int
        The number of nodes in K_'NODES'.
    verbose : bool, default False
        Choose how much output you want. 
    verify : bool, default False
        We proved that all node pairs that might conceivably be the result of 
        a 1-edge split are actually the result of a 1-edge split. If 'verify' 
        is True, we nonetheless check if the reverse node split actually 
        yields a facet inducing graph to be sure we made no errors in the 
        implementation. 

    Returns
    -------
    node_split_reversible : bool
        States whether (G_'vertex', 'T') results from a smaller facet inducing 
        Steiner graph via 1-edge split.
    '''
    edge_split_reversible = False
    
    contractible_edges = eligible_for_reverse_1_edge_split(vertex, T, NODES)
    if len(contractible_edges)>0:
        edge_split_reversible = True
    
    for edge in contractible_edges: 
        if verbose:
            print("     mergeable for edge", edge)
            
        if verify:
            merged_graph, new_T = edge_contraction(vertex, T, edge, NODES)
            if not defines_facet(merged_graph, new_T, NODES-1):
                print("Error_merg")
                break
            
    return edge_split_reversible



