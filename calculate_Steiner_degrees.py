# -*- coding: utf-8 -*-
"""
Compute and save the Steiner degrees, minimal facet inducing terminal sets and 
right-hand sides in minimum integer form for the vertices of subtour(K_n).
"""

from fractions import Fraction
from collections import Counter
from itertools import combinations
from tqdm import tqdm

from functions.evaluation_functions import get_all_terminal_options
from functions.evaluation_functions import get_T_Steiner_Cuts, get_normalizer
from functions.read_write_functions import get_vertices, write_csv


###############################################################################
# Set your parameters:  
    
# Choose the number of nodes in our complete graph. Must be between 6 and 11.
NODES = 8

# Choose how much output you want
verbose = False

###############################################################################
###############################################################################


# get the vertices as strings and floats
vertices_str = get_vertices('vertices_data/vertices_'+str(NODES)+'.txt')
vertices = [[float(Fraction(c)) for c in v_str] for v_str in vertices_str]



# calculate the right hand sides in minimum integer form 
Ineq_rhs = []
for v in vertices_str:
    multiplier, rhs = get_normalizer(v)
    Ineq_rhs.append(rhs)
write_csv('csv_data/K_' + str(NODES) +'_rhs_min_int_form.csv', 
          data = zip(Ineq_rhs))    
    


# get all terminal sets and characteristic vectors of cuts that divide them
T_coll, cutset_coll = get_T_Steiner_Cuts(NODES)

# collect the results 
Steiner_degrees = []
Terminal_sets_all_options = []

counter = 0
vertices_len = len(vertices)
for v in (tqdm(vertices) if not verbose else vertices):
    
    # special treatment for K_11[3244], the only Hamiltonian circuit
    if NODES == 11 and v==vertices[3244]:
        Terminal_sets_all_options.append(list(combinations(range(11), 3)))
        Steiner_degrees.append(3)
        continue
    
    # get the Steiner degree and all minimal facet inducing terminal sets    
    all_Ts, tau = get_all_terminal_options(T_coll, cutset_coll, vertex=v)
    Terminal_sets_all_options.append(all_Ts)
    Steiner_degrees.append(tau)
    
    if verbose :
        counter +=1
        print(counter,'/', vertices_len)
        print('vertex: ', v, '\n has Steiner degree ', tau, '.\n')
    
    
    
# save the results
write_csv('csv_data/K_' + str(NODES) +'_Steiner_degrees.csv',
              data = zip(Steiner_degrees))
write_csv('csv_data/K_' + str(NODES) +'_Terminal_sets_all_options.csv',
              data = zip(Terminal_sets_all_options))
    




# count and save the occurences of each Steiner degree
tau_collector = []
taus = range(6,NODES+1)
for i in taus:
    tau_collector.append(Steiner_degrees.count(i))     
write_csv('csv_data/K_' + str(NODES) +'_Steiner_stats.csv',
          data = zip(taus, tau_collector),
          header = ['Steiner degree', 'number of occurences'])


# count and save the occurences of each combination of Steiner degree and 
# right-hand side in minimum integer form
combos = []
for deg, rhs in zip(Steiner_degrees, Ineq_rhs):
    combos.append(str(deg) + ',' + str(rhs))
vals = [i for i in Counter(combos).values()]
keys = [i for i in Counter(combos).keys()]
write_csv('csv_data/K_' + str(NODES) +'_deg_rhs_combo.csv',
          data = zip(keys, vals),
          header = ['deg, rhs', 'number of occurences'] )


# output
print('\n')
print('We have', len(vertices), ' vertices of subtour(K_', NODES, ').')
for key, val in zip(keys, vals): 
    print('There are', val ,'vertices with Steiner degree', 
          key.split(',')[0], 
          'and right-hand side', key.split(',')[1], 
          'in min. int. form.')

    

    

