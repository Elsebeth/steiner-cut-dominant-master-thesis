# -*- coding: utf-8 -*-
"""
Create plots for the support graph of a given vertex of subtour(K_n).
All possible choices of minimal terminal sets can be displayed.
An interactive graph makes it easy to visualize operations on the graph.
"""

from fractions import Fraction


from functions.read_write_functions import get_vertices, get_all_terminal_sets
from functions.node_positions import get_pos
from functions.plot_functions import interactive_graph, draw_graph


###############################################################################
# Set your parameters:  

# Choose the number of nodes in our complete graph. Must be between 6 and 11.
NODES = 9

# Choose the index of the vertex whose support graph you want to draw. 
# (line 'nr_vertex' in vertices_'NODES'.text, starting with line 0)
nr_vertex = 14

# Set to True if you want to plot each possible choice of a minimal terminal 
# set T such that vertex*x >= 2 defines a facet of CUT_+(K_'NODES',T).
# Otherwise only one choice of T will be displayed.
all_Ts=True


# Set to True if you want an interactive support graph for the vertex that
# lets you move nodes around.
show_interactive_graph = True

###############################################################################
###############################################################################


# get the vertex as strings and as floats
vertex_str = get_vertices('vertices_data/vertices_'+str(NODES)+'.txt', 
                            nr_vertex)
vertex = [float(Fraction(c)) for c in vertex_str]

# get the terminal sets T for which the vertex defines a facet of 
# CUT_+(K_'NODES',T)
terminal_sets = get_all_terminal_sets(NODES, nr_vertex)
if not all_Ts:
    terminal_sets = [terminal_sets[0]]

    
    
    

# there are manually specified  positions for some vertices - 
# mainly for NODES<=10 and Steiner degree <=9 
pos = get_pos(NODES, nr_vertex)

# plot the graph with the terminal nodes highlighted 
for T in terminal_sets:
    print('terminal nodes:', T)
    draw_graph(vertex, vertex_str, T, NODES, nr_vertex = nr_vertex, 
           position = pos, title = True)




# create an interactive graph 
T = terminal_sets[0] # choose a set of terminal nodes
if show_interactive_graph:
    interactive_graph(vertex, vertex_str, NODES, T, show_edge_labels=True, 
                  highlight_1_edges=False)
# after moving the nodes around, the graph can be saved as a png by 
# right-clicking it
